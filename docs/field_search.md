# 外键字段搜索

`simplepro`中只是重写了相关字段的样式，基本的处理逻辑和`django`中的`admin`是一样的。

在很多需求中需要过滤外键的数据，所以我们可以使用django内置的方法来实现。

例如：

```python

class StudentManyToManyModel(models.Model):
    f = models.CharField(max_length=32, verbose_name='多对多')

    def __str__(self):
        return self.f


class ManyToManyModel(models.Model):
    name = fields.CharField(max_length=128, verbose_name='名字', default='张三')
    many_to_many = fields.ManyToManyField(StudentManyToManyModel, blank=True, verbose_name='多对多字段')

    class Meta:
        verbose_name = '多对多 Select'
        verbose_name_plural = '多对多 Select'

@admin.register(StudentManyToManyModel)
class StudentManyToManyModelAdmin(admin.ModelAdmin, SourceCodeAdmin):
    search_fields = ('f',)
    
    def get_search_results(self, request, queryset, search_term):
        queryset, use_distinct = super().get_search_results(request, queryset, search_term)
        if search_term:
            queryset |= self.model.objects.filter(f__contains=search_term)
        return queryset, use_distinct

```

在上列代码中`ManyToManyModel`的`many_to_many`字段是一个`ManyToManyField`，关联了`StudentManyToManyModel`，

在`StudentManyToManyModelAdmin`中我们重写了`get_search_results`方法，这个方法是在`search_fields`中的字段进行搜索时调用的，

我们可以在这个方法中添加自己的搜索逻辑，当然在form表单中进行增加、修改等操作也会调用。 

+ 如何判断是搜索还是增加、修改等操作呢？

可以通过`search_term`是否为空来判断。

+ 如何判断是列表页，还是表单页呢？

可以通过`request.path`来判断。

注：本文所介绍的不是simplepro的特性与功能，都是django的基本功能，所以在使用时可以参考django的文档。

[返回目录](#目录)
