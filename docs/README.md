---
home: true
heroImage: /images/image1.png
actionText: 快速上手 →
actionLink: doc.html
features:
- title: 简洁至上
  details: 以 SimpleUI 为基础，配合Element-UI和Vue，让页面不止进步一点。
- title: Vue驱动
  details: 享受 Vue +Element-UI 的开发体验，在项目中使用 Vue 组件，带来高效开发。
- title: 高性能
  details: SimplePro 以Ajax前后分离请求数据，避免页面资源重复加载，让性能提升一大步。
footer: Copyright © 2019-present Simple社区
---

## 官网
[https://simpleui.88cto.com/simplepro](https://simpleui.88cto.com/simplepro)

## 社区
[https://simpleui.88cto.com](https://simpleui.88cto.com)

如果您遇到使用的问题，欢迎到社区进行提问。

## 快速安装：

``` shell
pip3 install simplepro
```

## 在线demo

[https://simplepro.demo.72wo.com/](https://simplepro.demo.72wo.com/)

## demo源码

[https://github.com/newpanjing/simplepro_demo](https://github.com/newpanjing/simplepro_demo)