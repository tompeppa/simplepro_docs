# 3.0 更新

+ 完善对国际化的支持


+ 列表的筛选框可以支持多选

+ 菜单栏bug修复以及增加菜单折叠动画

+ 列表页图标更换

+ 列表页UI优化

> 现在安装simplepro非常简单，只需要在`settings.py`中加入simplepro即可。
> 然后执行迁移和菜单同步

```shell
python3 manage.py migrate simplepro
python3 manage.py syncmenu
```

+ 删除优化

删除功能将可以实现逻辑删除不必物理删除

如果想要自己处理删除逻辑，不需要simplepro直接删除数据，可以将`delete_queryset`方法返回`True`
这样simplepro就不会执行删除动作。可以自己实现一些逻辑删除

例如：
```python
def delete_queryset(self,request,queryset):
    queryset.filter(xxxx...).update(is_delete=False)
    return True
```
