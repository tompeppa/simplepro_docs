# 导入导出

simplepro导入导出是依赖 `django-import-export`插件，配置请参考该插件的文档。

## 插件主页
[https://github.com/django-import-export/django-import-export](https://github.com/django-import-export/django-import-export)

## 安装

1. pip安装

```shell
pip3 install django-import-export
```

2. INSTALL_APPS

在项目中的`settings.py`中的INSTALL_APPS中加入

```python
# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'import_export',
    .....
]

```


## 项目配置

在自定义按钮附近使用导出和右边工具栏使用导入导出：


```python

from import_export import resources
from import_export.admin import ImportExportModelAdmin, ImportExportActionModelAdmin

class ProxyResource(resources.ModelResource):
    class Meta:
        model = Employe

@admin.register(Employe)
class EmployeAdmin(ImportExportActionModelAdmin):
    resource_class = ProxyResource

```

## 常见问题

有关该插件的任何使用问题，请移步至作者Github主页咨询。SimplePro 与该插件无任何瓜葛。
[https://github.com/django-import-export/django-import-export](https://github.com/django-import-export/django-import-export)

或者您也可以异步至Simple社区寻求帮助：

[http://simpleui.88cto.com/](http://simpleui.88cto.com/)