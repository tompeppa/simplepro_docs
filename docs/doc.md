# SimplePro

## 介绍
SimplePro是一个基于django-simpleui的一个升级版框架，采用element-ui和vue，前后分离方式。兼容原生admin，功能更多界面更美观！
![](/images/image1.png)

## 支持的Python版本

|版本|说明|
|---|---|
|Python2.7.x|不支持|
|Python3.x|支持|
|Python3.7.x|支持|
|Python3.8.x|支持|
|Python3.9.x|支持|
|Python3.10.x|支持|
|Python3.11.x|支持|

> 不支持是指未经官方测试，可能某些模块会报错。

## 支持的Django版本
|版本|说明|
|---|---|
|Django1.x|不支持|
|Django2.x|支持|
|Django3.0.x|支持|
|Django3.1.x|支持|
|Django3.2.x|支持|
|Django3.3.x|支持|
|Django4.x|支持，`3.3`及以上版本|

> 不支持是指未经官方测试，可能某些模块会报错。

## 文档
+ [安装步骤](/setup.md)
+ [主页](/home.md)
+ [图表](/charts.md)
+ [自定义按钮](/action.md)
+ [自定义权限](/permissions.md)
+ [导入导出插件](/export_import.md)
+ [表格配置](/table.md)
+ [JS SDK](/jssdk.md)
+ [编辑器](/editor.md)

## 示例

+ 本地Demo-Github
[https://github.com/newpanjing/simplepro_demo](https://github.com/newpanjing/simplepro_demo)

+ 本地Demo-国内Gitee
[https://gitee.com/tompeppa/simplepro_demo](https://gitee.com/tompeppa/simplepro_demo)


+ 在线Demo
[http://simplepro.demo.72wo.com/](http://simplepro.demo.72wo.com/)

---

## pip源切换

pip默认的源服务器在国外，安装速度较慢，推荐使用国内的源。

### 源列表
- 豆瓣：http://pypi.douban.com/simple/
- 中科大：https://pypi.mirrors.ustc.edu.cn/simple/
- 清华：https://pypi.tuna.tsinghua.edu.cn/simple

### 一次性使用
可以在使用pip的时候加参数-i https://pypi.tuna.tsinghua.edu.cn/simple

例如：

```shell
pip install django-simpleui -i https://pypi.tuna.tsinghua.edu.cn/simple
```
### 永久修改

#### Linux & MacOS
linux下，修改 ~/.pip/pip.conf (没有就创建一个)， 修改 index-url 为国内镜像地址，内容如下：

```
[global]
index-url = https://pypi.tuna.tsinghua.edu.cn/simple
```

#### Windows
windows下，直接在user目录中创建一个pip目录，如：C:\Users\xx\pip，新建文件pip.ini，内容如下

```
[global]
index-url = https://pypi.tuna.tsinghua.edu.cn/simple
```
---

## 安装

simplepro 依赖 simpleui、django-export-import、以及django


1. 安装simplepro

```shell
pip install simplepro
```

2. INSTALL_APPS中配置

> 按以下顺序加入到INSTALL_APPS数组的顶部
```python
INSTALLED_APPS = [
    'simplepro',
    'simpleui',
    'import_export',
    ......
]    
```
3. 配置中间件

```python
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    # 加入simplepro的中间件
    'simplepro.middlewares.SimpleMiddleware'
]
```

+ 如果在开启DEBUG模式后，启动即可运行。
+ 如果关闭后，需要克隆静态文件，步骤与simpleui一致。[文档地址](https://github.com/newpanjing/simpleui/blob/master/QUICK.md#%E5%85%8B%E9%9A%86%E9%9D%99%E6%80%81%E6%96%87%E4%BB%B6%E5%88%B0%E6%A0%B9%E7%9B%AE%E5%BD%95)

## 预览图
### 自定义主题

<img :src="$withBase('/images/image2.png')" alt="foo">

### 搜索模块
<img :src="$withBase('/images/image4.png')" width='100%'>

### 前后端分离加载数据
<img :src="$withBase('/images/image5.png')" width='100%'>

### 编辑页面
<img :src="$withBase('/images/image6.png')" width='100%'>

### 导出数据
<img :src="$withBase('/images/image7.png')" width='100%'>

### 导入插件
<img :src="$withBase('/images/image8.png')" width='100%'>

### 导出插件
<img :src="$withBase('/images/image9.png')" width='100%'>

### 自定义权限
<img :src="$withBase('/images/image10.png')" width='100%'>


## 付费和激活
可以先直接安装simplepro，启动项目后打开后台会有相关提示页面，根据提示页面操作即可激活。

> Simple Pro是收费项目，如果不想付费请继续使用[Simpleui](https://github.com/newpanjing/simpleui)我们也会将开源免费的项目维护到底。收费的目的是为了让simpleui和simplepro能够更好的维护下去。

## 主要功能
以下列出的功能，均是在simpleui的基础上进行的

### 页面
- 首页改版
- 登录页改版
- 列表页改版
- 权限页改版
- 导入页和导出页改版
- 增加网页全局进度条
- 替换全局部分图标
- Pro版独有标识
- 专业版主题
- 优化最近动作和主题，改为侧栏显示
- 优化返回按钮，改为绝对路径 通过reverse('admin:demo_employe_add')
- 全新默认LOGO
- 首页增加图表
- Pro专属[组件](/components.html)

### 功能
- 自定义按钮ajax实现
- 筛选、删除、搜索、表格ajax加载数据
- 导入导出优化
- 权限模块重写
- 表格功能增加详见table.md文档
- 自定义主题

### 权限
- 可添加自定义权限
- 自定义按钮权限控制
- 自定义菜单权限控制
- 自定义字段权限

## FAQ常见问题

### 官方社区

您可以在官方社区找到一些常见问题的答案，也可以提出您遇到的问题。

[https://simpleui.72wo.com/](https://simpleui.72wo.com/)

### 在线购买

可以在线购买simplepr的激活，也可以在项目内进行购买，安装后会有相关提示页面。

[https://simpleui.72wo.com/simplepro](https://simpleui.72wo.com/simplepro)

### 离线激活

考虑到部分公司的内网环境，simplepro提供了[离线模式](#离线模式)和离线激活的方式。
请在simplepro官方购买后，到`我的订单->离线激活->下载激活文件`，然后放置项目内进行离线激活。

### 离线模式

> 在3.3.6以及以后版本中，离线模式已经正式取消，该属性将会被忽略。

离线默认是指simplepro默认不使用第三方cdn，采用读取本地静态文件的方式。

在项目的`settings.py`中加入：

```python
SIMPLEUI_STATIC_OFFLINE=True
```

### 插件兼容
+ django_apscheduler
+ django-import-export

以上两个插件做了特定的兼容，其他插件也可以兼容。


### 系统权限
+ PermissionError: [Errno 13] Permission denied

请请检查是否对python lib目录有访问权限，linux和Windows默认用普通用户，对全局python lib包没有写的权限，所有会提示权限问题。建议用虚拟环境运行。



### 隐藏 simplepro 版本和授权信息

请在settings.py中加入：
```python
SIMPLEPRO_INFO = False
```

### 无法获取套餐列表
 
 请检查是否能访问simpleui.72wo.com
 
 ```shell
ping simpleui.72wo.com
 ```

 如果被防火墙屏蔽，请采用hosts的方式，在hosts文件中加入：
 ```
103.39.210.34 simpleui.72wo.com
 ```

 ### QQ群
 群号：786576510
 <div style="text-align:center;">
 <img :src="$withBase('/images/qrcode.jpeg')" height="250">
 </div>

### 虚拟环境
在使用虚拟环境运行时，请务必使用虚拟环境的python，不要使用系统python，否则会出现权限问题。

+ 虚拟环境创建

```shell
python3 -m venv venv
```

+ 进入虚拟环境
```shell
source venv/bin/activate
```

+ 安装依赖
```shell
pip install -r requirements.txt
```

+ 启动项目

```shell
python3 manage.py runserver 8080
```

### 导入导出

导入导出采用的是 `django-import-export` 插件，可以[参考文档](https://django-import-export.readthedocs.io/en/stable/)

中文字段显示参考，[点击查看](https://www.shangmayuan.com/a/1f7928bb5a3046bc8bed3dbf.html)

例子：

```python
from import_export.fields import Field

class BookResource(resources.ModelResource):
    id = Field(attribute='id', column_name='编号')
    name = Field(attribute='name', column_name='书籍名称')

    class Meta:
        model = Book
        export_order = ('id', 'name', 'author', 'price',)
```

OR

```python
name = Field(attribute='name', column_name=Book.name.field.verbose_name)
```
### 首页和登录页面不同LOGO设置

[https://simpleui.72wo.com/topic/6946](https://simpleui.72wo.com/topic/6946) 感谢[宅记](https://simpleui.72wo.com/profile/283/posts/1)提供方法！

### BUG&建议

 我们在官网开辟了反馈专栏，您可以随时到[官网](http://simpleui.72wo.com/)进行BUG和改进建议的反馈。我们十分期待您与我们一起完善该项目。给大家提供良好的体验。