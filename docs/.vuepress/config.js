module.exports = {
  plugins: ['@vuepress/back-to-top'],
  title: 'Simple Pro',
  description: '一个前后分离的django admin框架',
  head: [ // 注入到当前页面的 HTML <head> 中的标签
    ['link', { rel: 'icon', href: '/images/logo.png' }],
    ['link', { rel: 'manifest', href: '/images/logo.png' }],
    ['link', { rel: 'apple-touch-icon', href: '/images/logo.png' }],
    ['meta', { 'http-quiv': 'pragma', cotent: 'no-cache' }],
    ['meta', { 'http-quiv': 'pragma', cotent: 'no-cache,must-revalidate' }],
    ['meta', { 'http-quiv': 'expires', cotent: '0' }],
    ["script", { async: true, src: "https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-9423996531680870", crossorigin: "anonymous" }]
  ],
  serviceWorker: true, // 是否开启 PWA
  base: '/docs/simplepro/', // 部署到github相关的配置
  markdown: {
    lineNumbers: true // 代码块是否显示行号
  },
  themeConfig: {
    nav: [ // 导航栏配置
      { text: '主页', link: '/' },
      {
        text: '文档',
        items: [
          {
            text: "新手入门",
            items: [
              { text: '介绍', link: '/doc' },
              { text: '购买PRO', link: 'https://simpleui.72wo.com/simplepro' },
              { text: '安装步骤', link: '/setup' },
              { text: '全局配置', link: '/config' },
              { text: '命令行', link: '/command' },
            ]
          },
          {
            text: "基础功能",
            items: [
              { text: '自定义按钮', link: '/action' },
              { text: '自定义权限', link: '/permissions' },
              { text: '导入导出插件', link: '/export_import' },
              { text: 'ModelAdmin配置', link: '/table' },
              { text: 'js&Vue-SDK', link: '/jssdk' },
              { text: 'PRO组件🔆', link: '/components' },
            ]
          },
          {
            text: "进阶功能",
            items: [
              { text: '可视化图表设计', link: 'https://sdc.72wo.com' },
              { text: '富文本编辑器', link: '/editor' },
              { text: '自定义弹出层Layer', link: '/layer' },
              { text: '单元格对话框', link: '/dialog' },
              { text: '单元格调用自定义按钮', link: '/cell_action' },
            ]
          },
          {
            text: "提问求助",
            items: [
              { text: '在线社区', link: 'https://simpleui.72wo.com' },
              { text: 'QQ群1：786576510', link: '/contact#1' },
              { text: 'QQ群2：873469913', link: '/contact#2' },
              { text: 'QQ群3：722755389', link: '/contact#3' },
            ]
          },
        ]
      },
      {
        text: '发布日志',
        items: [{
          text: "🚩v6.5",
          link: "/release/6.5"
        },{
          text: "v6.3",
          link: "/release/6.3"
        },{
          text: "v6.2",
          link: "/release/6.2"
        },{
          text: "v6.1",
          link: "/release/6.1"
        },{
          text: "v6.0",
          link: "/release/6.0"
        }, {
          text: 'v5.0',
          items: [
            {
              text: '5.4', link: '/release/5.4',
            },
            {
              text: '5.3', link: '/release/5.3',
            },
            {
              text: '5.2', link: '/release/5.2',
            }, {
              text: '5.1', link: '/release/5.1',
            }, {
              text: '5.0', link: '/release/5.0'
            }
          ]
        },
        {
          text: 'v4.0',
          items: [
            { text: '4.0.8', link: '/release/4.0.8' },
            { text: '4.0.7', link: '/release/4.0.7' },
            { text: '4.0', link: '/release/4.0' }
          ]
        }
        ]
      },
      { text: '社区&官网', link: 'https://simpleui.72wo.com' },
      { text: '在线Demo', link: 'http://simplepro.demo.72wo.com' },
      { text: 'B站教程', link: 'https://space.bilibili.com/351994239' },
      { text: '博客', link: 'http://www.88cto.com' },
    ],
    sidebar: 'auto', // 侧边栏配置
    sidebarDepth: 4,
    lastUpdated: '更新时间',
    editLinkText: '帮助我们改善此页面！',
    repo: 'https://gitee.com/tompeppa/simplepro_docs',
    repoLabel: '查看源码',
    editLinks: true,
    docsDir: 'docs',
    // 假如文档放在一个特定的分支下：
    docsBranch: 'master',
    //页面滚动
    smoothScroll: true
  }
};