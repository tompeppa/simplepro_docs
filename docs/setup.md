# 安装指南

simplepro 依赖 simpleui、django-export-import、以及django

## 1. 安装simplepro

```shell
pip install simplepro
```

## 2. INSTALL_APPS中配置

> 按以下顺序加入到INSTALL_APPS数组的顶部
```python
INSTALLED_APPS = [
    'simplepro',
    'simpleui',
    'import_export',
    ......
]    
```
## 3. 配置中间件

```python
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    # 加入simplepro的中间件
    'simplepro.middlewares.SimpleMiddleware'
]
```
## 4. >= django 3.2.X 可能需要加入这一句
> 有可能提示`sp`未定义或者图片组件上传失败，或者编辑器图片上传失败

在项目的`urls.py`中加入这句：
```python
path('sp/', include('simplepro.urls')),
```

完整例子，`urls.py`文件加入：

```python

urlpatterns = [
    path('admin', admin.site.urls),
    path('test', test),
    path('area/search', views.area_search, name='area_search'),
    # 这里可以配置网页收藏夹的图标
    path('favicon.ico', RedirectView.as_view(url=r'static/favicon.ico')),
    # 如果出现sp 未定义，就加入这一句
    path('sp/', include('simplepro.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

```

## 5.注意
+ 如果在开启DEBUG模式后，启动即可运行。
+ 如果关闭后，需要克隆静态文件，步骤与simpleui一致。[文档地址](https://github.com/newpanjing/simpleui/blob/master/QUICK.md#%E5%85%8B%E9%9A%86%E9%9D%99%E6%80%81%E6%96%87%E4%BB%B6%E5%88%B0%E6%A0%B9%E7%9B%AE%E5%BD%95)

## 6. 例子

如果您无法配置成功，可以参考[github](https://github.com/newpanjing/simplepro_demo)或者[gitee](https://gitee.com/tompeppa/simplepro_demo)上的demo示例。

或者加入QQ群：

    QQ群1：786576510
    QQ群2：873469913
    QQ群3：722755389

## 7. 项目模板

我们提供了一个机遇Django+SimplePro的模版，您只需要克隆下来，就可以直接在此基础上进行快速开发。

[https://github.com/newpanjing/django_project](https://github.com/newpanjing/django_project)
## 更多其他配置
[文档](home)